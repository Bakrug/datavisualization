import csv, os
import matplotlib.pyplot as plt

# Census Bureau designated regions and divisions
West = ['Washington','Oregon','California','Idaho','Nevada','Arizona','Utah','Montana','Wyoming','Colorado','New Mexico','Hawaii','Alaska']

MidWest = ['North Dakota', 'South Dakota', 'Nebraska', 'Kansas', 'Minnesota', 'Iowa', 'Missouri', 'Wisconsin', 'Illinois', 'Indiana', 'Michigan', 'Ohio']

South = ['Texas', 'Oklahoma', 'Arkansas', 'Louisiana', 'Mississippi', 'Alabama', 'Georgia', 'Florida', 'South Carolina', 'Tennessee', 'Kentucky', 'West Virginia', 'Virginia', 'North Carolina', 'Maryland', 'Delaware']

NorthEast= ['Pennsylvania', 'New Jersey', 'New York', 'Connecticut', 'Rhode Island', 'Vermont' , 'New Hampshire', 'Massachusetts', 'Maine', ]


# Gets the data from the data file
def getContents():
    with open(os.path.join(os.getcwd(), 'February_2017_Flight_Data.csv'), 'r') as csvFile:
        contents = csv.reader(csvFile, delimiter=',')
        stuff = []
        for row in contents:
            stuff.append(row)
        return stuff

def checkRow(contents, col):
    WestCount = MidWestCount = SouthCount = NorthEastCount = OtherCount = 0
    # We don't need the row of labels
    contents.pop(0)
    # I know there is probably a better way to do this
    for row in contents:
        found = False
        for state in West:
            if state == row[col]:
                WestCount = WestCount + 1
                found = True
                break
        if not found:
            for state in MidWest:
                if state == row[col]:
                    MidWestCount = MidWestCount + 1
                    found = True
                    break
        if not found:
            for state in South:
                if state == row[col]:
                    SouthCount = SouthCount + 1
                    found = True
                    break
        if not found:
            for state in NorthEast:
                if state == row[col]:
                    NorthEastCount = NorthEastCount + 1
                    found = True
                    break
        if not found:
            OtherCount = OtherCount + 1
    return [WestCount, MidWestCount, SouthCount, NorthEastCount, OtherCount]

def pie(nums, labels, colors):
    fig1, ax1 = plt.subplots()
    ax1.pie(nums, explode=(0,0,0,0,0), colors=colors, shadow=False, startangle=90)
    ax1.axis('equal')
    ax1.legend(labels, title="Example", loc="lower right")
    # We have to draw our own labels and arrows for better customization
    ax1.arrow(0.02,1.3, 0, -.22, head_width=.05, head_length=.05)
    ax1.text(-0.05, 1.35, "0.7%")
    ax1.text(-.70, 0.20, "33.6%")
    ax1.text(-.28, -0.63, "19.2%")
    ax1.text(.45, -0.15, "33.1%")
    ax1.text(.08, 0.60, "10.4%")
    plt.show()

def main():
    contents = getContents()
    nums = checkRow(contents, 16)
    pie(nums, ['West','Midwest','South','North East', 'Other'], ['#FFA8A8', '#FF8484', '#FF5B5B', '#FF1919', '#D10000'])

main()